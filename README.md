# Currents.dev - GitLab CI/CD Example

This is an example repository that showcases using Gitlab CI/CD with [Currents.dev](https://currents.dev).

The example [CI config file](https://gitlab.com/currents.dev/gitlab-currents-example/-/blob/main/.gitlab-ci.yml):

- runs 5 containers with cypress tests in parallel

- installs `@currents/cli` npm package

- Note: set your project is from [Currents.dev](https://app.currents.dev) in `cypress.json`

- Note: use CLI arguments to customize your cypress runs, e.g.: `currents run --parallel --record --key <your currents.dev key> --group groupA`

- Note: create a project, get your record key on [Currents.dev](https://app.currents.dev) and set `CURRENTS_RECORD_KEY` [GitLab CI/CD variable for the project](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)
